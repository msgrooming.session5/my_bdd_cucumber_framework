Feature: Trigger Post API

@Post_API_TestCases
Scenario: Trigger the Post API request with valid request parameters 
	Given Enter name and job in Post request body 
	When Send the request with Post payload 
	Then Validate Post status code 
	And Validate Post response body parameters 
	
@Patch_API_TestCases
Scenario: Trigger the Patch API request with valid request parameters 
	Given Enter name and job in Patch request body 
	When Send the request with Patch payload 
	Then Validate Patch status code 
	And Validate Patch response body parameters
	
@Put_API_TestCases
Scenario: Trigger the Put API request with valid request parameters 
	Given Enter name and job in Put request body 
	When Send the request with Put payload 
	Then Validate Put status code 
	And Validate Put response body parameters 
@Get_API_TestCases	
Scenario: Trigger the Get API request with valid request parameters 
	Given Enter name and job in Get request body 
	When Send the request with Get payload 
	Then Validate Get status code 
	And Validate Get response body parameters 
	
