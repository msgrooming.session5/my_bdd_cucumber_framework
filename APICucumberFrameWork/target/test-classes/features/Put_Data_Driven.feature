Feature: Trigger put API
Scenario Outline: Trigger the Put API request with valid request parameters 
	Given Enter "<Name>" and "<Job>" in Put request body 
	When Send the request with Put payload.
	Then Validate Put status code.
	And Validate Put response body parameters.
	
	Examples:	
			|Name |Job |
			|Tanmay|QAManager|
			|Samir|QA|
			|Arvind|SrQA|