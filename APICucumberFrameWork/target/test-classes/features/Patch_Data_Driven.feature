Feature: Trigger patch API
Scenario Outline: Trigger the Patch API request with valid request parameters 
	Given Enter "<Name>" and "<Job>" in Patch request body 
	When Send the request with Patch payload. 
	Then Validate Patch status code.
	And Validate Patch response body parameters.
	
	Examples:	
			|Name |Job |
			|Tanmay|QAManager|
			|Samir|QA|
			|Arvind|SrQA|