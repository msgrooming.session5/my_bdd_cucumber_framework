package Test_package;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import org.testng.Assert;
import org.testng.annotations.Test;

import API_common_methods.common_method_handle_API;
import endpoint_repository.put_endpoint_repository;
import io.restassured.path.json.JsonPath;
import request_repository.put_request_repository;
import utility_common_methods.handle_api_logs;
import utility_common_methods.handle_directory;

public class put_TC1 extends common_method_handle_API {
	@Test
	public static void executor() throws IOException {
		File log_dir=handle_directory.create_log_directory("put_TC1_logs");
		String requestBody = put_request_repository.put_requestBody();
		String endpoint = put_endpoint_repository.put_repository();
		for (int i = 0; i < 5; i++) {
			int statusCode = put_statusCode(requestBody, endpoint);
			System.out.println("put API triggered");
			System.out.println(statusCode);
			if (statusCode == 200) {
				String responseBody = put_responseBody(requestBody, endpoint);
				System.out.println(responseBody);
				handle_api_logs.evidence_creator(log_dir, "put_TC1", endpoint, requestBody, responseBody);
				put_TC1.validator(requestBody, responseBody);
				break;
			} else {
				System.out.println("put API expected status code 200 not found hence re-trying it");
			}

		}
	}

	public static void validator(String requestBody, String responseBody) {
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 8);
		JsonPath jsp_res = new JsonPath(responseBody);
		String res_name = jsp_res.getString("name");
		String res_id = jsp_res.getString("id");
		String res_job = jsp_res.getString("job");
		String res_createdate = jsp_res.getString("createdAt");
		res_createdate = res_createdate.substring(0, 8);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdate, expecteddate);
	}
}
