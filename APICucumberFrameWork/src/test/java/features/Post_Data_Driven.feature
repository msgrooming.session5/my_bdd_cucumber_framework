Feature:	Trigger post API 
Scenario Outline:	Trigger the Post API request with valid request parameters 
			Given Enter "<Name>" and "<Job>" in Post request body 
			When Send the request with Post payload.
			Then Validate Post status code.
			And Validate Post response body parameters.
	
Examples:	
			|Name |Job |
			|Tanmay|QAManager|
			|Samir|QA|
			|Arvind|SrQA|