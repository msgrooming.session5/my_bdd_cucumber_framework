package StepDefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import request_repository.post_request_repository;
import utility_common_methods.handle_directory;
import org.testng.Assert;
import API_common_methods.common_method_handle_API;
import endpoint_repository.post_endpoint_repository;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

public class Post_Step_Defination {
	static String requestBody;
	static int statusCode;
	static String responseBody;
	static String endpoint;
	static File logDir;
	
	@Given("Enter name and job in Post request body")
	public void enter_name_and_job_in_request_body() throws IOException {
		logDir = handle_directory.create_log_directory("Post_Log_dir");
		endpoint=post_endpoint_repository.post_endpoint();
		requestBody = post_request_repository.post_request();
	   
	}
	@When("Send the request with Post payload")
	public void send_the_request_with_payload() {
		statusCode = common_method_handle_API.post_statusCode(requestBody, endpoint);
		responseBody =  common_method_handle_API.post_responseBody(requestBody, endpoint);
		System.out.println(statusCode);
		System.out.println(responseBody);
	}
	
	@Then("Validate Post status code")
	public void validate_status_code() {
	    Assert.assertEquals(statusCode, 201);
	}
	
	@Then("Validate Post response body parameters")
	public void validate_response_body_parameters() {
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 8);
		JsonPath jsp_res = new JsonPath(responseBody);
		String res_name = jsp_res.getString("name");
		String res_id = jsp_res.getString("id");
		String res_job = jsp_res.getString("job");
		String res_createdate = jsp_res.getString("createdAt");
		res_createdate = res_createdate.substring(0, 8);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdate, expecteddate);
	}

}
