package StepDefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import utility_common_methods.handle_directory;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import API_common_methods.common_method_handle_API;
import endpoint_repository.get_endpoint_repository;
import java.io.File;
import java.io.IOException;

public class Get_Step_Definition {
	static int statusCode;
	static String responseBody;
	static String endpoint;
	static File logDir;
	
	@Given("Enter name and job in Get request body")
	public void enter_name_and_job_in_request_body() throws IOException {
		logDir = handle_directory.create_log_directory("Get_Log_dir");
		endpoint=get_endpoint_repository.get_request();
		}
	
	@When("Send the request with Get payload")
	public void send_the_request_with_payload() {
		statusCode = common_method_handle_API.get_statusCode(endpoint);
		responseBody =  common_method_handle_API.get_responseBody(endpoint);
		System.out.println(statusCode);
		System.out.println(responseBody);
	}
	
	@Then("Validate Get status code")
	public void validate_status_code() {
	    Assert.assertEquals(statusCode, 200);
	}
	
	@Then("Validate Get response body parameters")
	public void validate_response_body_parameters() {
		int expected_id[] = { 7, 8, 9, 10, 11, 12 };
		String expected_firstname[] = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String expected_lastname[] = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };
		String expected_email[] = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
									"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		JSONObject array_res = new JSONObject(responseBody);
		JSONArray dataarray = array_res.getJSONArray("data");
		int count = dataarray.length();
		for (int i = 0; i < count; i++) {
			int res_id = dataarray.getJSONObject(i).getInt("id");
			int exp_id = expected_id[i];
			String res_firstname = dataarray.getJSONObject(i).getString("first_name");
			String exp_firstname = expected_firstname[i];
			String res_lastname = dataarray.getJSONObject(i).getString("last_name");
			String exp_lastname = expected_lastname[i];
			String res_email = dataarray.getJSONObject(i).getString("email");
			String exp_email = expected_email[i];
			Assert.assertEquals(res_id, exp_id);
			Assert.assertEquals(res_firstname, exp_firstname);
			Assert.assertEquals(res_lastname, exp_lastname);
			Assert.assertEquals(res_email, exp_email);
			}

		}
}
